package ru.kpfu.itis.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.dto.ProductDto;
import ru.kpfu.itis.services.SearchService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.List;

import static ru.kpfu.itis.constants.Paths.PRODUCTS_LIVE_SEARCH_PATH;

@WebServlet(urlPatterns = {PRODUCTS_LIVE_SEARCH_PATH}, loadOnStartup = 1)
public class LiveSearchServlet extends HttpServlet {
    private ObjectMapper objectMapper;
    private SearchService searchService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.objectMapper = context.getBean(ObjectMapper.class);
        this.searchService = context.getBean(SearchService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws  IOException {
        String query = request.getParameter("query");
        List<ProductDto> products = searchService.searchProducts(query);
        String jsonResponse = objectMapper.writeValueAsString(products);
        response.setContentType("application/json");
        response.getWriter().write(jsonResponse);
    }
}

