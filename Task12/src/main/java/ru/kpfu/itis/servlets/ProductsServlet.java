package ru.kpfu.itis.servlets;


import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kpfu.itis.config.ApplicationConfig;
import ru.kpfu.itis.dto.ProductDto;
import ru.kpfu.itis.dto.converters.http.HttpFormsConverter;
import ru.kpfu.itis.models.Product;
import ru.kpfu.itis.services.ProductsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.kpfu.itis.constants.Paths.*;

@WebServlet(urlPatterns = {PRODUCTS_PATH, ADD_PRODUCT_PATH}, loadOnStartup = 1)
public class ProductsServlet extends HttpServlet {
    private HikariDataSource hikariDataSource;
    private ProductsService productsService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + PRODUCTS_PATH)) {
            resp.setContentType("text/html");
            List<ProductDto> products = productsService.getAllProducts();

            PrintWriter writer = resp.getWriter();

            StringBuilder html = new StringBuilder();
            //TODO extract to single class
            html.append("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "\t<meta charset=\"utf-8\">\n" +
                    "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "\t<title>Products</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<form action=\"/app/products/sortChange\" method=\"post\">\n" +
                    "<select name=\"sortBySelect\">\n" +
                    "<option value=\"\">Default</option>\n" +
                    "<option value=\"id\">ID</option>\n" +
                    "<option value=\"productName\">Name</option>\n" +
                    "<option value=\"price\">Price</option>\n" +
                    "<option value=\"count\">Count</option>\n" +
                    "</select>\n" +
                    "<input type=\"submit\" value=\"Sort\"/>\n" +
                    "</form>\n" +
                    "<table>\n" +
                    "\t<tr>\n" +
                    "\t\t<th>ID</th>\n" +
                    "\t\t<th>Product Name</th>\n" +
                    "\t\t<th>Price</th>\n" +
                    "\t\t<th>Count</th>\n" +
                    "\t\t<th>Color</th>\n" +
                    "\t</tr>");

            for (ProductDto product : products) {
                html.append("<tr>\n");
                html.append("<td>").append(product.getId()).append("</td>\n");
                html.append("<td>").append(product.getName()).append("</td>\n");
                html.append("<td>").append(product.getPrice()).append("</td>\n");
                html.append("<td>").append(product.getCount()).append("</td>\n");
                html.append("<td>").append(product.getColor()).append("</td>\n");
                html.append("</tr>\n");
            }

            html.append("</table>\n" +
                    "</body>\n" +
                    "</html>");

            writer.println(html);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + ADD_PRODUCT_PATH)) {
            ProductDto productDto = HttpFormsConverter.from(req);
            productsService.addProduct(productDto);
            resp.sendRedirect(APPLICATION_PREFIX + PRODUCTS_PATH);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }

    }

    private Cookie getCookieByName(String name, HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        Cookie cookie = null;
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (name.equals(c.getName())) {
                    cookie = c;
                    return cookie;
                }
            }
        }
        return null;
    }

    private List<ProductDto> getProductsList(HttpServletRequest req) {
        List<ProductDto> products;
        Cookie sortByCookie = getCookieByName("sortBy", req);
        String sortBy = "";
        if (sortByCookie != null) {
            sortBy = sortByCookie.getValue().trim();
        }
        switch (sortBy) {
            case ("id"):
                products = productsService.getAllProductsOrderById();
                break;
            case ("name"):
                products = productsService.getAllProductsOrderByName();
                break;
            case ("price"):
                products = productsService.getAllProductsOrderByPrice();
                break;
            case ("count"):
                products = productsService.getAllProductsOrderByCount();
                break;
            default:
                products = productsService.getAllProducts();
        }
        return products;
    }


    @Override
    public void destroy() {
        this.hikariDataSource.close();
    }
}
