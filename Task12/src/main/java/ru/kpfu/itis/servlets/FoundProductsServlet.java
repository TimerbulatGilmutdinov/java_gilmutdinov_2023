package ru.kpfu.itis.servlets;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kpfu.itis.config.ApplicationConfig;
import ru.kpfu.itis.models.Product;
import ru.kpfu.itis.services.ProductsService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static ru.kpfu.itis.constants.Paths.APPLICATION_PREFIX;
import static ru.kpfu.itis.constants.Paths.FOUND_PRODUCTS_PATH;

@WebServlet(FOUND_PRODUCTS_PATH)
public class FoundProductsServlet extends HttpServlet {
    private HikariDataSource hikariDataSource;
    private ProductsService productsService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        this.hikariDataSource = context.getBean(HikariDataSource.class);
        this.productsService = context.getBean(ProductsService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getRequestURI().equals(APPLICATION_PREFIX + FOUND_PRODUCTS_PATH)
                && req.getParameter("name") != null) {
            String name = req.getParameter("name").trim().toLowerCase();

            List<Product> products = productsService.getProductByNameLike(name);

            resp.setContentType("text/html");

            PrintWriter writer = resp.getWriter();

            StringBuilder html = new StringBuilder();

            html.append("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "\t<meta charset=\"utf-8\">\n" +
                    "\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "\t<title>Products</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<table>\n" +
                    "\t<tr>\n" +
                    "\t\t<th>ID</th>\n" +
                    "\t\t<th>Product Name</th>\n" +
                    "\t\t<th>Price</th>\n" +
                    "\t\t<th>Count</th>\n" +
                    "\t\t<th>Color</th>\n" +
                    "\t</tr>");

            for (Product product : products) {
                html.append("<tr>\n");
                html.append("<td>").append(product.getId()).append("</td>\n");
                html.append("<td>").append(product.getName()).append("</td>\n");
                html.append("<td>").append(product.getPrice()).append("</td>\n");
                html.append("<td>").append(product.getCount()).append("</td>\n");
                html.append("<td>").append(product.getColor()).append("</td>\n");
                html.append("</tr>\n");
            }

            html.append("</table>\n" +
                    "</body>\n" +
                    "</html>");

            writer.println(html);
        } else {
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    @Override
    public void destroy() {
        this.hikariDataSource.close();
    }
}
