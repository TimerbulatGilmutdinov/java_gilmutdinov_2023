package ru.kpfu.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserDto;
import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UsersRepository;
import ru.kpfu.itis.services.UsersService;

import java.util.List;

import static ru.kpfu.itis.dto.UserDto.from;


@Service
@RequiredArgsConstructor
public class UsersServiceImpl implements UsersService {
    private final UsersRepository usersRepository;
    @Override
    public void signUp(SignUpDto signUpUserData) {
        User user = User.builder()
                .firstName(signUpUserData.getFirstName())
                .lastName(signUpUserData.getLastName())
                .email(signUpUserData.getEmail())
                .password(signUpUserData.getPassword())
                .build();
        usersRepository.save(user);
    }

    @Override
    public List<UserDto> getAllUsers() {
        return from(usersRepository.findAll());
    }
}
