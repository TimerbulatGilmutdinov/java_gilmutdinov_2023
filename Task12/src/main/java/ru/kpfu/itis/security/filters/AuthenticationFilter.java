package ru.kpfu.itis.security.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static ru.kpfu.itis.constants.Paths.*;

@WebFilter("/*")
public class AuthenticationFilter implements Filter {
    public static String requestedURI;
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        for (String publicPage : getPublicPages()) {
            if (request.getRequestURI().equals(publicPage)) {
                filterChain.doFilter(request, response);
                return;
            }
        }

        requestedURI = request.getRequestURI();

        if (isAuthenticated(request)) {
            filterChain.doFilter(request, response);
            return;
        }

        response.sendRedirect(APPLICATION_PREFIX + SIGN_IN_PATH);
    }

    private List<String> getPublicPages() {
        List<String> publicPages = new ArrayList<>();
        publicPages.add(APPLICATION_PREFIX + START_PAGE);
        publicPages.add(APPLICATION_PREFIX + SIGN_IN_PATH);
        publicPages.add(APPLICATION_PREFIX + SIGN_UP_PATH);
        publicPages.add(APPLICATION_PREFIX + SIGN_UP_PAGE);
        return publicPages;
    }

    private boolean isAuthenticated(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            Boolean isAuthenticated = (Boolean) session.getAttribute("isAuthenticated");
            if (isAuthenticated != null) {
                return isAuthenticated;
            }
        }
        return false;
    }
}
