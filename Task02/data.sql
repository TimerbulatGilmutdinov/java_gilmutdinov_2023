insert into client(first_name, last_name, age) values ('Bulat','Gilmutdinov',19);
insert into client(first_name, last_name, age) values ('Marat','Gilmutdinov',50);
insert into client(first_name, last_name, age) values ('Nikita','Safin',24);
insert into driver(first_name, last_name, age) values ('Alexey','High',38);
insert into driver(first_name, last_name, age) values ('Ivan','Ivanov',34);
insert into driver(first_name, last_name, age) values ('Alex','Coll',27);
insert into car(model, colour) values ('Ford Mustang','black');
insert into car(model, colour) values ('Volkswagen Golf','White');
insert into car(model, colour) values ('Lada','Sedan');
insert into taxi_order(price, start_place, finish_place) values (1000,'Home','School');
insert into taxi_order(price, start_place, finish_place) values (340,'Fuchika street','Glushko street');
insert into taxi_order(price, start_place, finish_place) values (200,'Gogol street','Tukay street');

update client set phone_number = '89271111111', age = 19 where id = 1;
update client set phone_number = '89064445533', age = 50 where id = 3;
update client set phone_number = '89613128904', age = 24 where id = 2;
