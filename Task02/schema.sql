drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists taxi_order;


create table client(
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    age integer check (age>18 and age<120)
);

create table driver (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    age integer check(age>18 and age<120)
);

create table car (
    id bigserial primary key,
    model char(30),
    colour char(20)
);

create table taxi_order (
    id bigserial primary key,
    price integer check(price>0),
    start_place char(30),
    finish_place char(30)
);

alter table client add phone_number char(14) not null default '';
alter table car add number char(6) not null default '';
