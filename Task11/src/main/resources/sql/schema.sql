create table product
(
    id bigserial primary key,
    product_name varchar(20),
    price int,
    count int check ( count >= 0 ),
    color varchar(20)
);


drop table product;