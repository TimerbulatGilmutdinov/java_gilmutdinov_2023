package ru.kpfu.itis.services;

import ru.kpfu.itis.dto.ProductDto;

import java.util.List;

public interface SearchService {
    List<ProductDto> searchProducts(String query);
}
