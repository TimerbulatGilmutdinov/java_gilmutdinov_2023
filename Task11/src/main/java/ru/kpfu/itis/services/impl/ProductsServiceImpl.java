package ru.kpfu.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.dto.ProductDto;
import ru.kpfu.itis.models.Product;
import ru.kpfu.itis.repositories.ProductsRepository;
import ru.kpfu.itis.services.ProductsService;

import java.util.List;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    @Override
    public boolean addProduct(ProductDto productDto) {
        try {
            Product product = Product.builder()
                    .name(productDto.getName())
                    .price(productDto.getPrice())
                    .count(productDto.getCount())
                    .color(productDto.getColor())
                    .build();
            productsRepository.save(product);
            return true;
        } catch (DataIntegrityViolationException e) {
            return false;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public boolean deleteProduct(Long id) {
        return productsRepository.delete(id) != 0;
    }

    @Override
    public List<Product> getProductByNameLike(String name) {
        return productsRepository.findProductsByNameLike(name.toLowerCase());
    }
}

