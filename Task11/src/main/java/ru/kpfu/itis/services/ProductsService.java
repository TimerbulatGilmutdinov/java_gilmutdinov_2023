package ru.kpfu.itis.services;

import ru.kpfu.itis.dto.ProductDto;
import ru.kpfu.itis.models.Product;

import java.util.List;

public interface ProductsService {
    boolean addProduct(ProductDto productDto);
    List<Product> getAllProducts();
    boolean deleteProduct(Long id);
    List<Product> getProductByNameLike(String name);
}
