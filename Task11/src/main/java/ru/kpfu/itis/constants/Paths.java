package ru.kpfu.itis.constants;

public class Paths {
    public static final String APPLICATION_PREFIX = "/app";

    public static final String ADD_PRODUCT_PATH = "/addProduct";

    public static final String PRODUCTS_PATH = "/products";

    public static final String FOUND_PRODUCTS_PATH = "/foundProducts";

    public static final String PRODUCTS_LIVE_SEARCH_PAGE = "/liveSearchProduct.html";

    public static final String PRODUCTS_LIVE_SEARCH_PATH = "/liveSearchProduct";

}

