package ru.kpfu.itis.dto.converters.http;

import ru.kpfu.itis.dto.ProductDto;

import javax.servlet.http.HttpServletRequest;

public class HttpFormsConverter {
    public static ProductDto from(HttpServletRequest request) {
        return ProductDto.builder()
                .name(request.getParameter("name"))
                .price(Integer.parseInt(request.getParameter("price")))
                .count(Integer.parseInt(request.getParameter("count")))
                .color(request.getParameter("color"))
                .build();
    }
}
