package ru.kpfu.itis.dto;

import lombok.Builder;
import lombok.Data;
import ru.kpfu.itis.models.Product;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class ProductDto {
    private Long id;
    private String name;
    private Integer price;
    private Integer count;
    private String color;

    public static ProductDto from(Product product) {
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .count(product.getPrice())
                .color(product.getColor())
                .build();
    }

    public static List<ProductDto> from(List<Product> products) {
        return products.stream()
                .map(ProductDto::from)
                .collect(Collectors.toList());
    }
}

