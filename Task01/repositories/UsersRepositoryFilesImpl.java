package ru.kpfu.itis.Task01.repositories;

import ru.kpfu.itis.Task01.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;

    private static final Function<User, String> userToString = user ->
            user.getId()
                    + "|" + user.getFirstName()
                    + "|" + user.getLastName()
                    + "|" + user.getEmail()
                    + "|" + user.getPassword();

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                String[] userArgs = line.split("\\|");
                ArrayList<String> listForUserArgs = new ArrayList<>(Arrays.asList(userArgs));
                users.add(new User(
                        listForUserArgs.get(0),
                        listForUserArgs.get(1),
                        listForUserArgs.get(2),
                        listForUserArgs.get(3),
                        listForUserArgs.get(4))
                );
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return users;
    }

    @Override
    public void save(User entity) {
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {
            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
             BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            ArrayList<String> list = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                if (!line.substring(0, line.indexOf('|')).equals(entity.getId())) {
                    list.add(line);
                } else {
                    list.add(userToString.apply(entity));
                }
                line = reader.readLine();
            }
            for (String userInfo : list) {
                writer.write(userInfo);
                writer.newLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void delete(User entity) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
             BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            ArrayList<String> list = new ArrayList<>();
            String line = reader.readLine();

            while (line != null) {
                System.out.println(line );
                if (!line.substring(0, line.indexOf('|')).equals(entity.getId())) {
                    list.add(line);
                }
                line = reader.readLine();
            }
            for (String userInfo : list) {
                writer.write(userInfo);
                writer.newLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteById(String id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
             BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            ArrayList<String> list = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                if (!line.substring(0, line.indexOf('|')).equals(id)) {
                    list.add(line);
                }
                line = reader.readLine();
            }
            for (String userInfo : list) {
                writer.write(userInfo);
                writer.newLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public User findById(String id) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                String currentUserId = line.substring(0, line.indexOf('|'));
                String[] userArgs = line.split("\\|");
                ArrayList<String> list = new ArrayList<>(Arrays.asList(userArgs));
                if (currentUserId.equals(id)) {
                    return new User(currentUserId, list.get(1), list.get(2), list.get(3), list.get(4));
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }
}