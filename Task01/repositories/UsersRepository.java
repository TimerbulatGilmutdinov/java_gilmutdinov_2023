package ru.kpfu.itis.Task01.repositories;

import ru.kpfu.itis.Task01.models.User;

public interface UsersRepository extends CrudRepository<String, User> {
}
