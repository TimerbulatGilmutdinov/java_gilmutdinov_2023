package ru.kpfu.itis.Task01.app;

import ru.kpfu.itis.Task01.dto.SignUpForm;
import ru.kpfu.itis.Task01.mappers.Mappers;
import ru.kpfu.itis.Task01.models.User;
import ru.kpfu.itis.Task01.repositories.UsersRepository;
import ru.kpfu.itis.Task01.repositories.UsersRepositoryFilesImpl;
import ru.kpfu.itis.Task01.services.UsersService;
import ru.kpfu.itis.Task01.services.UsersServiceImpl;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        usersService.signUp(new SignUpForm("Марсель", "Сидиков",
                "sidikov.marsel@gmail.com", "qwerty007"));

        usersService.signUp(new SignUpForm("Bulat","Gilmutdinov","timerhelmut@gmail.com","123"));

        usersService.signUp(new SignUpForm("Иван", "Иванов",
                "cool@gmail.com", "ghbdtn"));

        usersRepository.delete(new User("Bulat","Gilmutdinov","timerhelmut@gmail.com","123"));
    }
}
