package ru.kpfu.itis.Task01.services;

import ru.kpfu.itis.Task01.dto.SignUpForm;

public interface UsersService {
    void signUp(SignUpForm signUpForm);
}
