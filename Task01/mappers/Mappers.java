package ru.kpfu.itis.Task01.mappers;

import ru.kpfu.itis.Task01.dto.SignUpForm;
import ru.kpfu.itis.Task01.models.User;

public class Mappers {
    public static User fromSignUpForm(SignUpForm form) {
        return new User(form.getFirstName(), form.getLastName(), form.getEmail(), form.getPassword());
    }
}
