package ui;

import models.Product;
import services.ProductsService;

import java.util.List;
import java.util.Scanner;

public class UI {
    private final Scanner sc = new Scanner(System.in);
    private final ProductsService productsService;
    public UI(ProductsService productsService){
        this.productsService = productsService;
    }
    public void start() {
        while (true) {
            printMainMenu();
            String command =  sc.nextLine();
            switch (command){
                case "1":
                    addCommandHelper();
                    break;
                case "2":
                    findAllProductsCommandHelper();
                    break;
                case "3":
                    deleteCommandHelper();
                    break;
                case "4":
                    System.exit(0);
                    break;
            }
        }
    }
    private void deleteCommandHelper(){
        System.out.println("Enter id : ");
        Long id = sc.nextLong();
        productsService.deleteProduct(id);
    }

    private void addCommandHelper(){
        System.out.println("Enter name of the product : ");
        String name = sc.nextLine();

        System.out.println("Enter price of the product : ");
        Integer price = sc.nextInt();

        System.out.println("Enter color of the product : ");
        String color = sc.nextLine();

        System.out.println("Enter count of the product : ");
        Integer count = sc.nextInt();

        productsService.addProduct(name,price,color,count);
    }

    private void findAllProductsCommandHelper(){
        List<Product> listOfProducts = productsService.findAllProducts();
        System.out.println("List of products : ");
        for(Product product:listOfProducts){
            System.out.println(product);
        }
    }

    private void printMainMenu(){
        System.out.println("1. Добавить товар");
        System.out.println("2. Посмотреть список товаров");
        System.out.println("3. Удалить товар по id");
        System.out.println("4. Выход");
    }
}
