package services;

import models.Product;

import java.util.List;

public interface ProductsService {
    List<Product> findAllProducts();
    void addProduct(String productName, Integer price, String color, Integer count);
    void deleteProduct(Long id);
}
