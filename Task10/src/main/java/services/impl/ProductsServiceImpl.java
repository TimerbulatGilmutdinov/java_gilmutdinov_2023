package services.impl;

import models.Product;
import repositories.ProductsRepository;
import services.ProductsService;

import java.util.List;

public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;

    public ProductsServiceImpl(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @Override
    public List<Product> findAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void addProduct(String productName, Integer price, String color, Integer count) {
        try {
            Product product = Product.builder()
                    .productName(productName)
                    .price(price)
                    .color(color)
                    .count(count)
                    .build();
            productsRepository.save(product);
        }catch (RuntimeException e){
            throw new IllegalArgumentException("One or more of arguments are invalid");
        }
    }


    @Override
    public void deleteProduct(Long id) {
        try{
            productsRepository.delete(id);
        }catch (RuntimeException e){
            throw new IllegalArgumentException("Invalid id, please try again");
        }
    }
}
