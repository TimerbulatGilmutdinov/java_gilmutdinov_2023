package models;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@ToString
public class Product {
    private Long id;
    private String productName;
    private Integer price;
    private String color;
    private Integer count;
}
