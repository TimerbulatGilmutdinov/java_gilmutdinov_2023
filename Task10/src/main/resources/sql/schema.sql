drop table if exists product;

create table product
(
    id         bigserial primary key,
    product_name varchar(20) not null ,
    price int not null ,
    color varchar(20) not null ,
    count int not null
);
