select model,speed,hd from PC where price<500;

select distinct maker from Product where type = 'Printer';

select model, ram, screen from Laptop where price>1000;

select * from Printer where color = 'y';

select distinct model, speed, hd from PC where cd = '12x' and price<600 or cd = '24x' and price<600;

select distinct p.maker, l.speed from laptop l join product p on p.model = l.model where l.hd >= 10;

select distinct product.model, pc.price from Product join pc on product.model = pc.model where maker = 'B'
union
select distinct product.model, laptop.price
from product join laptop ON product.model=laptop.model where maker='B'
union
select distinct product.model, printer.price
from product join printer ON product.model=printer.model where maker='B';

select distinct maker from product where type = 'pc' except select distinct product.maker from product where type = 'laptop';

select distinct maker from product p join pc on p.model=pc.model where pc.speed>=450;

select model, price from printer where price =(select max(price)from printer );

select avg(speed) from pc;

select avg(speed) from laptop where price>1000;

select avg(pc.speed) from pc, product where product.maker = 'A' and product.model = pc.model

select s.class, s.name, c.country from ships s left join classes c on s.class = c.class where c.numGuns >= 10;

select hd from PC group by hd having count(model) >= 2;

select distinct A.model as model, B.model as model, A.speed as speed, A.ram as ram from PC as A, PC B where A.speed = B.speed and A.ram = B.ram and A.model > B.model;

select distinct p.maker, pr.price from product p join printer pr on p.model = pr.model where pr.price = (select min(price) from printer where color = 'y') and pr.color = 'y'

select p.maker, avg(l.screen) from product p join laptop l on p.model = l.model group by p.maker;

select maker, count(model) as Count_Model from product where type = 'pc' group by maker having count(model) >= 3;

select p.maker, max(pc.price) as max_price from product p join pc pc on p.model = pc.model group by maker;

select speed, avg(price) from pc where speed > '600' group by speed;

select p.maker from product p join pc pc on p.model = pc.model where pc.speed >= '750' intersect Select p.maker from product p join laptop l on p.model = l.model where l.speed >= '750';

select distinct p.maker from product p join pc on p.model = pc.model where pc.ram = (select min(ram) from pc) and pc.speed = (select max(speed) FROM pc where ram = (select min(ram) from pc)) and p.maker in (select maker from product where type = 'printer');

select ship from outcomes where result = 'sunk' and battle = 'North Atlantic';

select distinct name from classes, ships where launched >=1922 and displacement>35000 and type='bb' and ships.class = classes.class

select distinct c.class from classes c join outcomes o on c.class = o.ship
union
select distinct c.class from classes c join ships s on c.class = s.class where s.class = s.name;

select country from classes where type = 'bb'
intersect
select country from classes where type = 'bc';

select distinct maker, max(type) as type from product
group by maker
having count(distinct type) = 1 and count(model) > 1;

select ship, battle from outcomes where result = 'sunk';

select distinct ship, displacement, numguns from classes
    left join ships on classes.class=ships.class right join outcomes on classes.class=ship or ships.name=ship where battle='Guadalcanal';

select s.name from ships s join classes c on s.name=c.class or s.class = c.class where c.bore = 16
union
select o.ship from outcomes o join classes c on o.ship=c.class where c.bore = 16;

select distinct o.battle from ships s join outcomes o on s.name = o.ship where s.class = 'kongo';

select s.name from ships s join classes c on s.class = c.class where country = 'japan' and (numGuns >= '9' or numGuns is null) and (bore < '19' or bore is null) and (displacement <= '65000' or displacement is null) and type='bb';

select c.class, min(s.launched) from classes c left join ships s on c.class = s.class group by c.class;
