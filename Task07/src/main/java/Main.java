import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import models.Product;
import repositories.ProductsRepository;
import repositories.ProductsRepositoryJdbcTemplateImpl;

import java.io.IOException;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.username"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        Product redmiBook = Product.builder()
                .productName("Xiaomi RedmiBook")
                .price(63500)
                .productCount(1)
                .color("White")
                .build();

        productsRepository.save(redmiBook);

        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findById(1L));
        System.out.println(productsRepository.findAllByPriceGreaterThanOrderByIdDesc(60000));
    }
}
