package repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import models.Product;
import javax.sql.DataSource;
import java.util.*;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL_PRODUCTS = "select * from product";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from product where id = :id";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_PRICE_GREATER_THAN_ORDER_BY_ID_DESC = "select * from product" +
            " where price > :minPrice order by id desc";

    //language=SQL
    private static final String SQL_UPDATE_BY_ID = "update product set product_name = :product_name, price = :price, " +
            "product_count = :count, color = :color where id = :id";

    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from product where id = :id";


    private static final RowMapper<Product> productMapper = (row, rowNumber) -> Product.builder()
            .id(row.getLong("id"))
            .productName(row.getString("product_name"))
            .price(row.getInt("price"))
            .productCount(row.getInt("count"))
            .color(row.getString("color"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public Optional<Product> findById(Long id) {
        try {
            return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                    Collections.singletonMap("id", id),
                    productMapper));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }

    }

    @Override
    public void update(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("id", product.getId());
        paramsAsMap.put("product_name", product.getProductName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("count", product.getProductCount());
        paramsAsMap.put("color", product.getColor());

        namedParameterJdbcTemplate.update(SQL_UPDATE_BY_ID, paramsAsMap);
    }

    @Override
    public void delete(Long id) {
        namedParameterJdbcTemplate.update(SQL_DELETE_BY_ID, Collections.singletonMap("id", id));
    }

    @Override
    public List<Product> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, productMapper);
    }

    @Override
    public void save(Product product) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("product_name", product.getProductName());
        paramsAsMap.put("price", product.getPrice());
        paramsAsMap.put("count", product.getProductCount());
        paramsAsMap.put("color", product.getColor());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("product")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();

        product.setId(id);
    }

    @Override
    public List<Product> findAllByPriceGreaterThanOrderByIdDesc(int minPrice) {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE_GREATER_THAN_ORDER_BY_ID_DESC, Collections.singletonMap("minPrice", minPrice), productMapper);
    }
}
