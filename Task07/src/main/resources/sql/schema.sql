drop table if exists product;

create table product
(
    id         bigserial primary key,
    product_name varchar(20),
    price int,
    color varchar(20),
    product_count int
);
