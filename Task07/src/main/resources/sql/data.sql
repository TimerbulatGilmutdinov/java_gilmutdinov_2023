insert into product(product_name, price, product_count, color)
VALUES ('Laptop', 65000, 5, 'White'),
       ('Mouse', 2500, 350, 'Black'),
       ('Keyboard', 1800, 350, 'Black'),
       ('Display', 15000, 20, 'Uncoloured');
