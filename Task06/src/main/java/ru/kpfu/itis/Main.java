package ru.kpfu.itis;

import ru.kpfu.itis.jdbc.SimpleDataSource;
import ru.kpfu.itis.models.Student;
import ru.kpfu.itis.repositories.StudentsRepository;
import ru.kpfu.itis.repositories.StudentsRepositoryJdbcImpl;
import ru.kpfu.itis.services.StudentsService;
import ru.kpfu.itis.services.StudentsServiceImpl;

import javax.sql.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {


    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(Main.class.getResourceAsStream("/db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl(dataSource);
        StudentsService studentsService = new StudentsServiceImpl(studentsRepository);

        Student student = studentsRepository.findById(1L).orElseThrow(IllegalArgumentException::new);
        System.out.println(student);
    }
}


