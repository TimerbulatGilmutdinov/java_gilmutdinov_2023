package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 11.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class StudentSignUp {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
}

