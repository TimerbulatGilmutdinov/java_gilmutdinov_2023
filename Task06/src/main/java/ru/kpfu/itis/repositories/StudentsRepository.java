package ru.kpfu.itis.repositories;

import ru.kpfu.itis.models.Student;

import java.util.List;
import java.util.Optional;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface StudentsRepository {
    List<Student> findAll();

    void save(Student student);

    Optional<Student> findById(Long id);

    void update(Student student);

    void delete(Long id);

    List<Student> findAllByAgeGreaterThanOrderByIdDesc(Integer minAge);
}

