package ru.kpfu.itis.services;

import ru.kpfu.itis.dto.StudentSignUp;
import ru.kpfu.itis.models.Student;
import ru.kpfu.itis.repositories.StudentsRepository;


public class StudentsServiceImpl implements StudentsService {

    private final StudentsRepository studentsRepository;

    public StudentsServiceImpl(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @Override
    public void signUp(StudentSignUp form) {
        Student student = new Student(form.getFirstName(),
                form.getLastName(),
                form.getEmail(),
                form.getPassword());

        studentsRepository.save(student);
    }
}

