import Annotations.ColumnName;
import Annotations.TableName;

@TableName(name = "accounts")
public class User {
    @ColumnName(name ="id", primary = true, identity = true)
    private Long id;
    @ColumnName(name = "first_name", maxLength = 25)
    private String firstName;
    @ColumnName(name = "last_name", maxLength = 25)
    private String lastName;
    @ColumnName(name = "is_worker", defaultBoolean = true)
    private boolean isWorker;

    public User(Long id, String firstName, String lastName, Boolean isWorker) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.isWorker = isWorker;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", isWorker=" + isWorker +
                '}';
    }
}
