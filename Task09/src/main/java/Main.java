public class Main {
    public static void main(String[] args){
        SQLGenerator sqlGenerator = new SQLGenerator();
        User user = new User(12L, "Bulat", "Gilmutdinov", true);
        System.out.println(sqlGenerator.createTable(User.class));
        System.out.println(sqlGenerator.insert(user));
    }
}
