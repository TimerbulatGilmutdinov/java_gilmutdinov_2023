import Annotations.ColumnName;
import Annotations.TableName;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class SQLGenerator {

    Map<Class<?>, String> aliases = new HashMap<>();
    private final String errorMessageForString = "ERROR: you have to input valid maxlength for varchar type";

    private void putAliases(){
        aliases.put(String.class, "varchar");
        aliases.put(Integer.class, "int");
        aliases.put(Long.class, "bigint");
        aliases.put(Double.class, "bigserial");
        aliases.put(boolean.class, "boolean");
        aliases.put(float.class, "float4");
        aliases.put(short.class, "int2");
    }

    public <T> String createTable(Class<T> entityClass){
        StringBuilder query = new StringBuilder();

        TableName tableName = entityClass.getDeclaredAnnotation(TableName.class);

        query.append("create table ")
                .append(tableName.name())
                .append(" ")
                .append("(\n");

        Field[] fields = entityClass.getDeclaredFields();
        putAliases();

        for(Field field:fields){
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            query.append(columnName.name()).append(" ");
            query.append(aliases.get(field.getType()));
            query.append(" ");

            int maxLength = columnName.maxLength();

            if(field.getType().equals(String.class)){
                if(maxLength > 0){
                    query.append("(").append(maxLength).append(")").append(" ");
                } else {
                    throw new IllegalArgumentException(errorMessageForString);
                }
            }
            if(columnName.identity()){
                query.append("generated always as identity").append(" ");
            }
            if(columnName.primary()){
                query.append("primary key").append(" ");
            }
            if(columnName.defaultBoolean()){
                query.append("default")
                        .append(" ")
                        .append(columnName.defaultBooleanValue())
                        .append(" ");
            }
            query.append(",\n");
        }
        query.deleteCharAt(query.lastIndexOf(","));
        query.append(");");
        return query.toString();
    }
    public String insert(Object entity){
        StringBuilder query = new StringBuilder();

        TableName tableName = entity.getClass().getDeclaredAnnotation(TableName.class);
        query.append("insert into")
                .append(" ")
                .append(tableName.name())
                .append(" ")
                .append("(");
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            ColumnName columnName = field.getAnnotation(ColumnName.class);
            if (columnName == null || (columnName.name().equals("id") && columnName.primary())) {
                continue;
            }
            query.append(columnName.name()).append(",");
        }
        query.deleteCharAt(query.lastIndexOf(","));

        query.append(")").append(" values ").append("(");
        for (Field field : fields) {
            field.setAccessible(true);
            ColumnName columnName = field.getAnnotation(ColumnName.class);

            if (columnName == null || columnName.name().equals("id")&& columnName.primary()) {
                continue;
            }
            try {
                String value = field.get(entity).toString();

                if (field.getType() == String.class) {
                    value = "'" + value + "'";
                }
                query.append(value);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
            query.append(",");
            field.setAccessible(false);
        }
        query.deleteCharAt(query.lastIndexOf(","));
        query.append(");");
        return query.toString();
    }
}
