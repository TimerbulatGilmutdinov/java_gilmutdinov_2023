package ru.kpfu.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.models.FileInfo;

import java.io.InputStream;
import java.nio.file.Path;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FileDto {
    private Long size;
    private String originalFileName;
    private String storageFileName;
    private String mimeType;
    private String description;
    private InputStream fileStream;
    private Path path;

    public static FileDto from(FileInfo fileInfo){
        return FileDto.builder()
                .size(fileInfo.getSize())
                .originalFileName(fileInfo.getOriginalFileName())
                .storageFileName(fileInfo.getStorageFileName())
                .mimeType(fileInfo.getMimeType())
                .description(fileInfo.getDescription())
                .build();
    }
}
