package ru.kpfu.itis.security.filters;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.security.AuthenticationManager;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.constants.Paths.*;

@WebFilter(SIGN_IN_PATH)
public class SignInFilter implements Filter {
    private AuthenticationManager authenticationManager;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        ApplicationContext context = (ApplicationContext) filterConfig.getServletContext().getAttribute("springContext");
        this.authenticationManager = context.getBean(AuthenticationManager.class);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (request.getMethod().equals("POST")) {
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            if (authenticationManager.authenticate(email, password)) {
                HttpSession session = request.getSession(true);
                session.setAttribute("isAuthenticated", true);
                session.setAttribute("email",email);
                if (AuthenticationFilter.requestedURI != null) {
                    response.sendRedirect(AuthenticationFilter.requestedURI);
                } else {
                    response.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
                }
            } else {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }


    }
}
