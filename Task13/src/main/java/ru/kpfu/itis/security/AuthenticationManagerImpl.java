package ru.kpfu.itis.security;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UsersRepository;

@Component
@RequiredArgsConstructor
public class AuthenticationManagerImpl implements AuthenticationManager{
    private final UsersRepository usersRepository;
    @Override
    public boolean authenticate(String email, String password) {
        User user = usersRepository.findByEmail(email).orElse(null);
        return user.getPassword().equals(password) && user!=null;
    }
}
