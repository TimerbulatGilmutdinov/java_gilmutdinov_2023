package ru.kpfu.itis.security.filters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static ru.kpfu.itis.constants.Paths.*;

@WebFilter(urlPatterns = {SIGN_IN_PAGE, SIGN_IN_PATH})
public class AuthenticatedUserFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession session = req.getSession(false);

        if (session != null) {
            Boolean isAuthenticated = (Boolean) session.getAttribute("isAuthenticated");
            if (isAuthenticated != null && isAuthenticated) {
                res.sendRedirect(APPLICATION_PREFIX + PROFILE_PATH);
                return;
            }
        }
        chain.doFilter(req, res);
    }

}
