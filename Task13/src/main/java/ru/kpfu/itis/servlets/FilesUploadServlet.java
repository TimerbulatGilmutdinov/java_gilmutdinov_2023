package ru.kpfu.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.dto.FileDto;
import ru.kpfu.itis.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.kpfu.itis.constants.Paths.FILES_UPLOAD_PATH;

@WebServlet(FILES_UPLOAD_PATH)
@MultipartConfig
public class FilesUploadServlet extends HttpServlet {

    private FilesService filesService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/uploadAvatar.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String description = (new BufferedReader(
                new InputStreamReader(request.getPart("description").getInputStream())).readLine());

        Part filePart = request.getPart("file");

        FileDto uploadedFileInfo = FileDto.builder()
                .size(filePart.getSize())
                .description(description)
                .mimeType(filePart.getContentType())
                .originalFileName(filePart.getSubmittedFileName())
                .fileStream(filePart.getInputStream())
                .build();

        filesService.upload(uploadedFileInfo);
    }
}
