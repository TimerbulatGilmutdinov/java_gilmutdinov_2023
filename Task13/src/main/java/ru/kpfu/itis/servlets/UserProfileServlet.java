package ru.kpfu.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.models.User;
import ru.kpfu.itis.repositories.UsersRepository;
import ru.kpfu.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.NoSuchElementException;

import static ru.kpfu.itis.constants.Paths.USER_PROFILE_PATH;

@WebServlet(USER_PROFILE_PATH)
public class UserProfileServlet extends HttpServlet {
    private UsersRepository usersRepository;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersRepository = context.getBean(UsersRepository.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = usersRepository.findByEmail((String) req.getSession().getAttribute("email")).orElseThrow(NoSuchElementException::new);
        req.setAttribute("user", user);
        req.getServletContext().getRequestDispatcher("/userProfile.jsp").forward(req, resp);
    }
}
