package ru.kpfu.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.dto.FileDto;
import ru.kpfu.itis.services.FilesService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import static ru.kpfu.itis.constants.Paths.FILES_PATH;

@WebServlet(FILES_PATH)
@MultipartConfig
public class FilesDownloadServlet extends HttpServlet {
    private FilesService filesService;
    private List<String> imageExtensions;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.filesService = context.getBean(FilesService.class);
        setImageExtensions();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String storageFileName = request.getParameter("fileName");
        FileDto file = filesService.getFile(storageFileName);

        response.setContentType(file.getMimeType());
        response.setContentLength(file.getSize().intValue());
        response.setHeader("Content-Disposition", "filename=\"" + file.getOriginalFileName() + "\"");
        Files.copy(file.getPath(), response.getOutputStream());
        response.flushBuffer();
    }

    private void setImageExtensions(){
        imageExtensions.add("image/jpeg");
        imageExtensions.add("image/png");
        imageExtensions.add("image/jpg");
    }

}
