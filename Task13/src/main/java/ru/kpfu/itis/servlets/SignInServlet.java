package ru.kpfu.itis.servlets;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.dto.SignInDto;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.converters.http.HttpFormsConverter;
import ru.kpfu.itis.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static ru.kpfu.itis.constants.Paths.*;

@WebServlet(SIGN_IN_PATH)
public class SignInServlet extends HttpServlet {
    private UsersService usersService;

    @Override
    public void init(ServletConfig config) {
        ApplicationContext context = (ApplicationContext) config.getServletContext().getAttribute("springContext");
        this.usersService = context.getBean(UsersService.class);
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/signIn.html").forward(req, resp);
    }
}
