package ru.kpfu.itis.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.dto.ProductDto;
import ru.kpfu.itis.repositories.ProductsRepository;
import ru.kpfu.itis.services.SearchService;

import java.util.Collections;
import java.util.List;

import static ru.kpfu.itis.dto.ProductDto.from;

@RequiredArgsConstructor
@Service
public class SearchServiceImpl implements SearchService {
    private final ProductsRepository productsRepository;

    @Override
    public List<ProductDto> searchProducts(String name) {
        if (name == null || name.equals("")) {
            return Collections.emptyList();
        }
        return from(productsRepository.findProductsByNameLike(name));
    }
}
