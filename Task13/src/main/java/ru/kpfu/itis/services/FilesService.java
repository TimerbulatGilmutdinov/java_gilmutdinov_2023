package ru.kpfu.itis.services;

import ru.kpfu.itis.dto.FileDto;

public interface FilesService {
    void upload(FileDto file);
    FileDto getFile(String fileName);
}
