package ru.kpfu.itis.services;

import ru.kpfu.itis.dto.SignInDto;
import ru.kpfu.itis.dto.SignUpDto;
import ru.kpfu.itis.dto.UserDto;
import ru.kpfu.itis.models.User;

import java.util.List;

public interface UsersService {
    List<UserDto> getAllUsers();
    void signUp(SignUpDto signUpUserData);
    boolean signIn(SignInDto signInUserData);
}
