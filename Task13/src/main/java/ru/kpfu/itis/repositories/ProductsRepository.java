package ru.kpfu.itis.repositories;

import ru.kpfu.itis.models.Product;

import java.util.List;
import java.util.Optional;


import java.util.List;
import java.util.Optional;

public interface ProductsRepository {
    Optional<Product> findById(Long id);
    void update(Product product);
    int delete(Long id);
    void save(Product product);
    List<Product> findAll();
    List<Product> findAllByPriceGreaterThanOrderByIdDesc(int minPrice);
    List<Product> findProductsByNameLike(String name);
    List<Product> findAllProductsOrderById();
    List<Product> findAllProductsOrderByName();
    List<Product> findAllProductsOrderByCount();
    List<Product> findAllProductsOrderByPrice();
}
