package ru.kpfu.itis.repositories;


import ru.kpfu.itis.models.FileInfo;

import java.util.List;
import java.util.Optional;

public interface FilesRepository {
    void save(FileInfo fileInfo);
    Optional<FileInfo> findByStorageFileName(String fileName);
    List<String> findAllStorageNamesByType(String... types);
}
