package ru.kpfu.itis.repositories;

import ru.kpfu.itis.models.User;

import java.util.List;
import java.util.Optional;

public interface UsersRepository {
    void save(User user);
    void delete(Long id);
    Optional<User> findById(Long id);
    Optional<User> findByEmail(String email);
    List<User> findAll();
    List<User> findAllByFirstNameOrLastNameLike(String query);
}
