package ru.kpfu.itis.repositories;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.models.User;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class UsersRepositoryJdbcTemplateImpl implements UsersRepository{
    private static final String SQL_SELECT_ALL_USERS = "select * from users;";
    private static final String SQL_SELECT_BY_EMAIL = "select * from users where email = :email";
    private static final String SQL_SELECT_BY_ID = "select * from users where id = :id";
    private static final String SQL_LIKE_BY_FIRST_NAME_OR_LAST_NAME = "select * from users where " +
            "(first_name ilike :query or last_name ilike :query)";
    private static final String SQL_DELETE_USER_BY_ID = "delete from users where id =:id";



    private static final RowMapper<User> userMapper = (row, rowNumber) -> User.builder()
            .id(row.getLong("id"))
            .firstName(row.getString("first_name"))
            .lastName(row.getString("last_name"))
            .email(row.getString("email"))
            .password(row.getString("password"))
            .build();

    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UsersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }


    @Override
    public void save(User user) {
        Map<String, Object> paramsAsMap = new HashMap<>();

        paramsAsMap.put("first_name", user.getFirstName());
        paramsAsMap.put("last_name", user.getLastName());
        paramsAsMap.put("email", user.getEmail());
        paramsAsMap.put("password", user.getPassword());

        SimpleJdbcInsert insert = new SimpleJdbcInsert(namedParameterJdbcTemplate.getJdbcTemplate());

        Long id = insert.withTableName("users")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(new MapSqlParameterSource(paramsAsMap)).longValue();
        user.setId(id);
    }


    @Override
    public void delete(Long id) {
        namedParameterJdbcTemplate.update(SQL_DELETE_USER_BY_ID, Collections.singletonMap("id", id));
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_ID,
                Collections.singletonMap("id", id),
                userMapper));
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(namedParameterJdbcTemplate.queryForObject(SQL_SELECT_BY_EMAIL,
                Collections.singletonMap("email", email),
                userMapper));
    }

    @Override
    public List<User> findAll() {
        return namedParameterJdbcTemplate.query(SQL_SELECT_ALL_USERS, userMapper);
    }

    @Override
    public List<User> findAllByFirstNameOrLastNameLike(String query) {
        return namedParameterJdbcTemplate.query(SQL_LIKE_BY_FIRST_NAME_OR_LAST_NAME,
                Collections.singletonMap("query", "%" + query + "%"),
                userMapper);
    }
}
