<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>User profile</title>
</head>
<body>
<h3>Your profile</h3>
<c:if test="${not empty user.avatarId}">
    <img src="${pageContext.request.contextPath}/images?id=${user.avatarId}"
         style="width: 150px"/>
</c:if>
<p>${user.firstName}</p>
<p>${user.lastName}</p>
<a href="${pageContext.request.contextPath}/files/upload"><button>Upload avatar</button></a>
<a href="${pageContext.request.contextPath}/profile"><button>Go Back</button></a>
</body>
</html>
