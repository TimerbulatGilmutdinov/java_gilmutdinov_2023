create table product
(
    id           bigserial primary key,
    product_name varchar(20),
    price        int,
    count        int check ( count >= 0 ),
    color        varchar(20)
);

create table users
(
    id         bigserial primary key,
    first_name varchar(30),
    last_name  varchar(30),
    email      varchar(30),
    password   varchar(30),
    avatar_id bigint references file_info(id)
);

create table file_info
(
    id                 bigserial primary key,
    original_file_name varchar(1000),
    storage_file_name  varchar(100),
    size               bigint,
    mime_type          varchar(50),
    description        varchar(1000)

);
