import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


@Mojo(name = "list-of-sizes", defaultPhase = LifecyclePhase.COMPILE)
public class ListOfSizesMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.sourceDirectory}", required = true, readonly = true)
    private String sourceFolderFileName;

    @Parameter(defaultValue = "${project.build.outputDirectory}", required = true)
    private String outputFileName;

    @Parameter(defaultValue = "listOfSizesFileName", required = true)
    private String listOfSizesFileName;

    @Override
    public void execute() throws MojoExecutionException {
        File outputFolder = new File(outputFileName);

        File listOfClassesFile = new File(outputFolder, listOfSizesFileName);


        try (BufferedWriter writer = new BufferedWriter(new FileWriter(listOfClassesFile))) {
            getLog().info("Output file for sizes of classes is - " + listOfSizesFileName);

            Files.walk(Paths.get(sourceFolderFileName))
                    .filter(Files::isRegularFile)
                    .forEach(file -> {
                        try {
                            writer.write(file.getFileName().toString() + " : "
                                    + toSize(file.toFile().length()));
                            writer.newLine();
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                    });

            getLog().info("Finish work");

        } catch (IOException e) {
            throw new MojoExecutionException(e);
        }
    }

    private String toSize(long size) {
        int i = 0;
        while (size >= 1024) {
            size /= 1024;
            i++;
        }
        String[] trimmedSizes = {"Bytes", "KB", "MB", "GB", "EB"};
        if (i > 4) {
            return "EB";
        } else {
            return trimmedSizes[i];
        }
    }
}
