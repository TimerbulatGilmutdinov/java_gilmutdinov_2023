select *
from student;

select *
from student
order by age desc, id;

select first_name, last_name
from student;

select c.title as course_title
from course c
where c.id in (
    select sc.course_id
    from student_course sc
    where sc.student_id in (select s.id from student s where s.first_name = 'Айрат'));

select distinct(lesson.name)
from lesson;

select l.name as lesson_name, count(*) as count
from lesson l
group by l.name;


select *
from course c
         left join lesson l on c.id = l.course_id;


select *
from course c
         right join lesson l on c.id = l.course_id;


select *
from course c
         inner join lesson l on c.id = l.course_id;

select *
from course c
         full outer join lesson l on c.id = l.course_id;




