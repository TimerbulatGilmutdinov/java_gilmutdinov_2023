package ru.kpfu.itis.repositories;

import ru.kpfu.itis.models.Student;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

/**
 * 08.07.2022
 * 03. Database
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class StudentsRepositoryJdbcImpl implements StudentsRepository {
    private final DataSource dataSource;
    private static final String SQL_SAVE_STUDENT = "insert into student(id,first_name,last_name,age)";
    private static final String SQL_SELECT_ALL_STUDENTS = "select * from student;";

    private static final Function<ResultSet, Student> studentMapper = row -> {

        try {
            Long id = row.getLong("id");
            String firstName = row.getString("first_name");
            String lastName = row.getString("last_name");
            Integer age = row.getObject("age", Integer.class);
            return new Student(id, firstName, lastName, age);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    };

    public StudentsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public List<Student> findAll() {

        List<Student> students = new ArrayList<>();

        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {

            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL_STUDENTS)) {
                while (resultSet.next()) {
                    students.add(studentMapper.apply(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

        return students;
    }

    @Override
    public void save(Student student) {
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement()) {
            Long id = student.getId();
            String firstName = student.getFirstName();
            String lastName = student.getLastName();
            Integer age = student.getAge();
            String saveStudentQuery = SQL_SAVE_STUDENT + "('" + id + "', '" + firstName + "', '" + lastName + "', " + age + ");";
            statement.executeUpdate(saveStudentQuery);
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Optional<Student> findById(Long id) {
        List<Student> students = findAll();
        for (Student student : students) {
            if (Objects.equals(student.getId(), id)) {
                return Optional.of(student);
            }
        }
        return Optional.empty();
    }
}

