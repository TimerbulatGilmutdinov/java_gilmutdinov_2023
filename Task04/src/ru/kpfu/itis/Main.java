package ru.kpfu.itis;

import ru.kpfu.itis.jbdc.SimpleDataSource;
import ru.kpfu.itis.models.Student;
import ru.kpfu.itis.repositories.StudentsRepository;
import ru.kpfu.itis.repositories.StudentsRepositoryJdbcImpl;

import javax.activation.DataSource;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("resources\\db.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        DataSource dataSource = (DataSource) new SimpleDataSource(
                properties.getProperty("db.url"),
                properties.getProperty("db.username"),
                properties.getProperty("db.password")
        );

        StudentsRepository studentsRepository = new StudentsRepositoryJdbcImpl((javax.sql.DataSource) dataSource);

        Student student = new Student("Bulat", "Gilmutdinov", 18);
        studentsRepository.save(student);

        System.out.println(studentsRepository.findById(5L));

    }
}
